﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour {

	public Transform tilePrefab;
    [Range(0,1)]
    public float tileOffsetPercent;
    List<Coord> tiles;
    public Map[] maps;
    public int mapIndex;
    Map map;

	void Start(){
		GenerateMap ();
	}

	public void GenerateMap(){
        map = maps[mapIndex];
        tiles = new List<Coord>();

        string mapName = "Generated Map";
        Transform mapTransform = new GameObject(mapName).transform;
        if (transform.FindChild(mapName)) {
            //editor calls this so use immediate
            DestroyImmediate(transform.FindChild(mapName).gameObject);
        }
        mapTransform.parent = transform;
		for (int x = 0; x < map.size.x; x++) {
			for (int y = 0; y < map.size.y; y++) {
				Vector3 tilePosition = new Vector3(-map.size.x/2f + 0.5f + x, 0, -map.size.y/2f + 0.5f + y);
				//wtf is euler??
				//vector3.right is x axis
				Transform tile = Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right*90)) as Transform;
                tile.localScale = Vector3.one * (1 - tileOffsetPercent);
                tile.parent = mapTransform;
                tiles.Add(new Coord(x, y));

            }	
		}
	}

    [System.Serializable]
    public struct Coord {
        public int x;
        public int y;
        public Coord(int _x, int _y) {
            x = _x;
            y = _y;
        }
    }

    [System.Serializable]
    public class Map {
        public Coord size;
        public Coord center {
            get {
                return new Coord(size.x/2, size.y/2);
            }
        }
    }
}
