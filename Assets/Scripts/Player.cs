﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (PlayerController))]
public class Player : MonoBehaviour {
	public float speed = 5f;
	PlayerController controller;
	Camera camera;
	void Start () {
		//assume playercontroller is attached to same game object
		controller = GetComponent<PlayerController> ();
		camera = Camera.main;
	}
	// Update is called once per frame
	void Update () {
		//this is to move with arrows
		//raw doesnt do any smoothing
		Vector3 moveInput = new Vector3 (Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
		//Debug.Log (moveInput);
		Vector3 moveVelocity = moveInput.normalized * speed;
		controller.Move (moveVelocity);

		Ray ray = camera.ScreenPointToRay (Input.mousePosition);
		Plane groundPlane = new Plane (Vector3.up, Vector3.zero);
		float rayDistance;

		if (groundPlane.Raycast (ray, out rayDistance)) {
			Vector3 point = ray.GetPoint(rayDistance);
			//point on plane where cursor is
			Debug.DrawLine(ray.origin, point, Color.red);
			controller.LookAt(point);
			//controller.Move (point.normalized);
			if (Input.GetMouseButtonDown (0)) {
				//left click
				controller.MoveTo(point);
			}
		}
	}
}
