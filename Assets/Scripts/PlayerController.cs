﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (Rigidbody))]
public class PlayerController : MonoBehaviour {
	Vector3 _velocity;
	Rigidbody rigidbody;
	Vector3 fromPosition;
	Vector3 toPosition;
	public float delay;

	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	public void Move(Vector3 velocity){
		_velocity = velocity;
	}
	public void LookAt(Vector3 point){
		point.y = transform.position.y;
		transform.LookAt (point);
	}
	public void MoveTo(Vector3 postion){
		StopCoroutine ("MoveMe");
		fromPosition = transform.position;
		toPosition = new Vector3 (postion.x, 1, postion.z);
		StartCoroutine("MoveMe", delay);
	}
	public void FixedUpdate(){
		rigidbody.MovePosition (rigidbody.position + _velocity * Time.fixedDeltaTime);
	}
	IEnumerator MoveMe(float delay){
		yield return new WaitForSeconds(delay); // start at time X
		// Time.time contains current frame time, so remember starting point
		float startTime = Time.time; 
		while (Time.time - startTime <= 1) {
			//execute until a second passes
			transform.position = Vector3.Lerp(fromPosition, toPosition, Time.time - startTime);
			//wait for next frame
			yield return 1;
		}
	}
}
